import commands.observe as observe


class CommandException(Exception):
    pass


def dispatch(mqtt_client, command):
    split_command = command.split()
    if split_command[0] == "observe":
        if len(split_command) < 2:
            raise CommandException("No params given")
        observe_command(mqtt_client, split_command[1])
    else:
        raise CommandException("No such command: "+split_command[0])


def observe_command(mqtt_client, resource):
    observe.execute(mqtt_client, sensor=resource, interval=1)
