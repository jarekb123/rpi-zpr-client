import sensors.sensors as sensors
import sensors.observer as observer


def execute(mqtt_client, **kwargs):
    if kwargs["sensor"] == "temp":
        obs = observer.Observer("temp_listener", "pi/temp", mqtt_client, sensors.read_temperature)
        obs.observe(kwargs["interval"])
