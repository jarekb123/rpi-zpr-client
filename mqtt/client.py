import paho.mqtt.client as paho
import commands.dispatcher as dispatcher


class MqttClient:
    def __init__(self, hostname, client_id, port, message_dispatch_func):
        self.hostname = hostname
        self.clientId = client_id
        self.port = port
        self.client_paho = paho.Client(client_id)

        self.client_paho.on_connect = self.on_connect
        self.client_paho.on_message = self.on_message
        self.client_paho.on_subscribe = self.on_subscribe
        self.client_paho.on_disconnect = self.on_disconnect
        self.connected = False

        self.message_dispatch_func = message_dispatch_func

    def connect(self):
        self.client_paho.connect(self.hostname, self.port)

    def publish(self, topic, payload):
        if self.connected:
            (result, mid) = self.client_paho.publish(topic, "fdd")
            self.client_paho.publish("response", "test")
            print paho.error_string(result)
        else:
            print "Not connected"

    def on_message(self, client, userdata, message):
        print "Received message (topic: "+message.topic+", qos="+str(message.qos)+", hostname="+self.hostname+"): "+str(message.payload)
        try:
            self.message_dispatch_func(self, message.payload)
        except dispatcher.CommandException as exception:
            print exception.message

    def on_connect(self, client, userdata, flags, rc):
        self.client_paho.subscribe("pi/#", 0)
        self.connected = True
        print "Connected to: "+self.hostname

    def on_disconnect(self, client, userdata, rc):
        self.connected = False
        if rc != 0:
            print "Unexpected disconnection, host=" + self.hostname

    def on_subscribe(self, client, username, mid, granted_qos):
        return "Subscribed (host="+self.hostname+"): "+str(mid)+" "+str(granted_qos)

    def loop(self):
        self.client_paho.loop_start()
