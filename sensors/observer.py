import sched
import time


class Observer:
    def __init__(self, name, mqtt_topic, mqtt_client, sensor_function, **kwargs):
        self.name = name
        self.mqtt_topic = mqtt_topic
        self.mqtt_client = mqtt_client
        self.sensor_f_kwargs = kwargs
        self.sensor_function = sensor_function
        self.observing = False
        self.scheduler = None

    def update(self):
        if len(self.sensor_f_kwargs) != 0:
            payload = self.sensor_function(self.sensor_f_kwargs)
        else:
            payload = self.sensor_function()
        self.mqtt_client.publish(self.mqtt_topic, payload)

    def observe(self, interval):
        self.update()
        print "Observe start, interval " + str(interval)
        if self.scheduler is None:
            self.scheduler = sched.scheduler(time.time, time.sleep)
        self.scheduler.enter(interval, 1, self.observe, (interval,))
        if self.observing is False:
            self.observing = True
            self.scheduler.run()
