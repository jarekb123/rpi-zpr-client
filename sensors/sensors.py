import subprocess


def read_temperature():
    return float(subprocess.check_output("cat /sys/class/thermal/thermal_zone0/temp", shell=True))
