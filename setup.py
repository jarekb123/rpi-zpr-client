from setuptools import setup

setup(
    name='rasppi_client',
    version='v0.1',
    packages=['mqtt', 'sensors', 'commands'],
    url='https://gitlab.com/jarekb123/rpi-zpr-client',
    license='',
    author='jaroslaw',
    author_email='j.butajlo@stud.elka.pw.edu.pl',
    description='',
    install_requires=['paho-mqtt']
)
