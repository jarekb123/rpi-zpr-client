import unittest
import commands.dispatcher as dispatcher
import mqtt.client as mqtt


class CommandsTestCase(unittest.TestCase):
    def __init__(self):
        # ustawienie klienta mqtt - adresy z mojej sieci lokalnej
        self.mqtt_client = mqtt.MqttClient("192.168.0.235", "rpi", 1883)

    def test_wrong_command_dispatch(self):
        with self.assertRaises(dispatcher.CommandException) as context:
            dispatcher.dispatch(None, "get light")
        self.assertTrue("No such command: get" in context.exception)

    def test_no_params_command(self):
        with self.assertRaises(dispatcher.CommandException) as context:
            dispatcher.dispatch(None, "observe")
        self.assertTrue("No params given")


if __name__ == '__main__':
    unittest.main()
