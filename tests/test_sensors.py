import unittest
import subprocess
import sensors.sensors as sensors


class SensorsTestCase(unittest.TestCase):
    def test_read_temperature(self):
        try:
            sensors.read_temperature()
        except subprocess.CalledProcessError:
            self.fail("CalledProcessError")


if __name__ == "__main__":
    unittest.main()
